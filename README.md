# 💡 Gitpod : create a GitLab Runner

A short project to create a GitLab Runner when a Gitpod workspace is created.

⚠️ To use it, you have to fork the project and declare a Gitpod variable nammed `MY_CICD_REGISTRATION_TOKEN` and contain [a GitLab Access Tokens](https://gitlab.com/-/profile/personal_access_tokens).

[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://gitlab.com/jeanphi-baconnais/gitpod-create-gitlab-runner)


## 👋 Contributions

Contributions are welcome. Please create an issue and ping me to assign you the ticket before working on it 🙏

